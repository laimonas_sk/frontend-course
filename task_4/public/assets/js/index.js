const section = document.querySelector('.products');
const carousel = document.querySelector('.slider');
const price = document.querySelector('.prices');

const db = 'http://localhost:3000/items';
const dbSlider = 'http://localhost:3000/slider';

const fetchItems = () => {
    Promise.all([
        fetch(db),
        fetch(dbSlider)
    ]).then(function (responses) {
        return Promise.all(responses.map(function (response) {
            return response.json();
        }));
    }).then(function (data) {
        data[0].forEach(product => {
            product = `
                <div class="item">
                    <a href="edit_item.html?productId=${product.id}" class="item-link">
                        <figure>
                            <img class="fig" src="${product.image}" alt=""/>
                            <figcaption class="title">${product.title}</figcaption>
                        </figure>
                        <div class="price">
                            <span class="buy">Buy now</span>
                            <span class="amount">€ ${product.price}</span>
                        </div>
                    </a>
                </div>
                `
            if (section) {
                section.insertAdjacentHTML('afterbegin', product);
            }
        });

        data[1].forEach(slide => {
            slide = `
                <div class="item item-main">
                    <a href="edit_item.html?productId=${slide.id}" class="item-link">
                        <figure>
                            <img class="fig" src="${slide.image}" alt=""/>
                            <figcaption class="title">${slide.title}</figcaption>
                        </figure>
                        <div class="price">
                            <span class="buy">Buy now</span>
                            <span class="amount">€ ${slide.price}</span>
                        </div>
                    </a>
                </div>
                `
            if (carousel) {
                carousel.insertAdjacentHTML('afterbegin', slide);
            }
        });
        console.log(data);
    }).catch(function (error) {
        console.log(error);
    });
}

window.onload = () => {
    fetchItems();
}
