const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const productId = urlParams.get('productId');
const img = document.querySelector('#edit_image');
const title = document.querySelector('#edit_title');
const price = document.querySelector('#edit_price');
const editImage = document.querySelector('.figImage');
const editConfirm = document.querySelector('#edit_confirm');
const deleteBtn = document.querySelector('#edit_delete');
const db = 'http://localhost:3000/items/';

const fetchProduct = () => {
    fetch(db + productId, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json()
        })
        .then(data => {
            console.log('Success:', data);
            title.value = data.title;
            img.value = data.image;
            price.value = data.price;
            editImage.innerHTML = `<img class="fig" src="${data.image}" alt=""/>`;

        })
        .catch((error) => {
            console.error('Error:', error);
        })
}

const editProduct = (e) => {
    e.preventDefault();

    const title = document.querySelector('#edit_title').value;
    const img = document.querySelector('#edit_image').value;
    const price = document.querySelector('#edit_price').value;

    fetch(db + productId, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            title: title,
            image: img,
            price: price
        })
    })
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            console.log(data);
            alert('Product updated successfully');
            window.location = '/'
        })
        .catch((error) => {
            console.error('Error:', error);
        })
}

const deleteProduct = (e) => {
    e.preventDefault();

    fetch(db + productId, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            console.log(data);
            alert('Item removed');
            window.location = '/'
        })
        .catch((error) => {
            console.error('Error:', error);
        })
}

window.onload = () => {
    fetchProduct();
}

editConfirm.addEventListener('click', editProduct);
deleteBtn.addEventListener('click', deleteProduct);