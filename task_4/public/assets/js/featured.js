let index = 0;
window.show = function (increase) {
    let carouselItems = document.querySelectorAll('.carousel .item');
    index = index + increase;
    index = Math.min(Math.max(index, 0), carouselItems.length - 1);
    carouselItems[index].scrollIntoView({behavior: 'smooth', block: 'nearest'});
    if (index === carouselItems.length - 1) {
        index = -1;
    }
}
