let clear = document.getElementById('clear');
let confirm = document.getElementById('confirm');
let form = document.querySelector('.form');

let db = 'http://localhost:3000/items';
let dbSlider = 'http://localhost:3000/slider';

const addItem = async (e) => {
    e.preventDefault();

    const inputTitle = document.querySelector('#title').value;
    const inputPrice = document.querySelector('#price').value;
    const inputImage = document.querySelector('#image').value;

    form.reset();

    if (inputTitle.length !== 0 && inputPrice.length !== 0 && inputImage.length !== 0) {
        try {
            const response = await fetch(db, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    title: inputTitle,
                    price: inputPrice,
                    image: inputImage
                })
            })
            if (!response.ok) {
                throw new Error('Network response was no');
            }
            const data = await response.json();
            console.log(data);
            alert('Product added successfully')
            window.location = '/'
        } catch (err) {
            console.log(err);
        }
    }
}

function clearForm() {
    form.reset();
}

clear.addEventListener("click", clearForm);
confirm.addEventListener('click', addItem);
