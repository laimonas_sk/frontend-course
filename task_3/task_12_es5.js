var Calculator = function (num) {
    this.num = num;
}

Calculator.prototype = {
    add: function (num) {
        this.num += num;
        return this;
    },

    subtract: function (num) {
        this.num -= num;
        return this;
    },

    divide: function (num) {
        this.num /= num;
        return this;
    },

    multiply: function (num) {
        this.num *= num;
        return this;
    }
}

let calc = new Calculator(0);
let amount = calc.add(5).multiply(2);

console.log(amount.num);
