let array = [1, 2, 3, 4, 5, 6, 7, 10];

let total = 0;
array.forEach(sum = (i) => total += i);

console.log(total);
