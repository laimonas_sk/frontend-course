let numbers = [10, 6, [4, 8], 3, [6, 5, [9]]];

function arraySum(arr) {
    let sum = 0;
    arr.map((item) => Array.isArray(item) ? sum += arraySum(item) : sum += item);
    return sum;
}

console.log(arraySum(numbers));
