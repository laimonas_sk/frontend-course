let colorsArr = require('colors');

let colors = [
    colorsArr.red,
    colorsArr.green,
    colorsArr.yellow,
    colorsArr.blue
];

let hash = '#';
let prevColor;

for (let i = 1; i <= 7; i++) {
    prevColor = randomColor();
    console.log(prevColor(hash.repeat(i)));
}

function randomColor() {
    let randColor = colors[Math.floor(Math.random() * colors.length)];

    if (prevColor === randColor) {
        return randomColor();
    }
    return randColor;
}
