class Calculator {
    constructor() {
        this.num = 0;
    }
    add (num) {
        this.num += num;
        return this;
    }

    subtract (num) {
        this.num -= num;
        return this;
    }

    divide (num) {
        this.num /= num;
        return this;
    }

    multiply (num) {
        this.num *= num;
        return this;
    }
}

let calc = new Calculator(0);
let amount = calc.add(5).multiply(2).divide(4);

console.log(amount.num);
